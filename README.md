Notes
==============================

Write notes in notebooks.

Author
------

- Jonathan Ling
- License MIT

Requirements
------------

- Kanboard >= 1.0.35

Installation
------------

You have the choice between 3 methods:

1. Install the plugin from the Kanboard plugin manager in one click
2. Download the zip file and decompress everything under the directory `plugins/Notes`
3. Clone this repository into the folder `plugins/Notes`

Note: Plugin folder is case-sensitive.

Documentation
-------------

TODO.

Notes
-------------
 - [ ] Implement dynamic tagging from edit page
 - [ ] Implement search by tag
 - [ ] Implement TODO list items
